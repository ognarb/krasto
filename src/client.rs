/*
 *   Copyright 2019 Carl Schwan <carl@carlschwan.eu>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation; either version 2 of
 *   the License or (at your option) version 3 or any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use qmetaobject::*;
use elefren::{prelude::*, registration::Registered};
use elefren::http_send::{HttpSend, HttpSender};
use crate::account::Account;


#[derive(QObject, Default)]
pub struct Client {
    base: qt_base_class!(trait QObject),

    // Properties
    account: qt_property!(Account; NOTIFY account_changed), 

    // Signals
    account_changed: qt_signal!(),
    
    // Methods
    verify_credential: qt_method!(fn verify_credential(&mut self) {
        // todo
    }),
}
