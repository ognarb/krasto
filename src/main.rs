extern crate qmetaobject;
extern crate elefren;
extern crate toml;

mod authentification;
mod clients;
mod client;
mod account;

use crate::authentification::Authentification;
use qmetaobject::*;
use std::ffi::CStr;

qrc!(qressource, "/" {
        "src/qml/Main.qml" as "Main.qml",
        "src/qml/AuthentificationPage.qml" as "AuthentificationPage.qml",
    }
);

fn main() {
    qml_register_type::<Authentification>(CStr::from_bytes_with_nul(b"Authentification\0").unwrap(), 1, 0, CStr::from_bytes_with_nul(b"Authentification\0").unwrap());
    qressource();

    let mut engine = QmlEngine::new();
    engine.load_file("qrc:/Main.qml".into());
    engine.exec();
}
