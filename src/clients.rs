/*
 *   Copyright 2019 Carl Schwan <carl@carlschwan.eu>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation; either version 2 of
 *   the License or (at your option) version 3 or any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use qmetaobject::*;
use qmetaobject::listmodel::QAbstractListModel;
use elefren::{prelude::*, registration::Registered};
use elefren::http_send::{HttpSend, HttpSender};
use crate::client::Client;


#[derive(QObject, Default)]
pub struct Clients {
    base: qt_base_class!(trait QAbstractListModel),
    data: Vec<Client>,
}

impl QAbstractListModel for Clients {
    fn row_count(&self) -> i32 {
        self.data.len() as i32
    }
    fn data(&self, index: QModelIndex, role:i32) -> QVariant {
        if role != USER_ROLE { return QVariant::default(); }
        // We use the QGadget::to_qvariant function
        self.data.get(index.row() as usize).map(|x|x.to_qvariant()).unwrap_or_default()
    }
    fn role_names(&self) -> std::collections::HashMap<i32, QByteArray> {
        vec![(USER_ROLE, QByteArray::from("value"))].into_iter().collect()
    }
}
