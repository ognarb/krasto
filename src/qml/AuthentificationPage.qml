import QtQuick 2.1
import QtQuick.Controls 2.5
import org.kde.kirigami 2.7 as Kirigami
import Authentification 1.0

Kirigami.Page {
    title: qsTr("Login")

    Authentification {
        id: authentification
    }

    Kirigami.FormLayout {
        width: parent.width
        visible: authentification.auth_url.length === 0

        Kirigami.ActionTextField {
            id: instance_url
            Kirigami.FormData.label: qsTr("Instance:")
        }

        Button {
            text: qsTr("Ok")
            onClicked: authentification.update_instance(instance_url.text);
        }
    }

    Kirigami.FormLayout {
        width: parent.width
        visible: authentification.auth_url.length !== 0

        Text {
            text: "To complete your authentification, click on this <a href='" + authentification.auth_url + "'>link</a>."
            onLinkActivated: Qt.openUrlExternally(link)
            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.NoButton // we don't want to eat clicks on the Text
                cursorShape: parent.hoveredLink ? Qt.PointingHandCursor : Qt.ArrowCursor
            }
        }

        Kirigami.ActionTextField {
            id: authorization_code
            Kirigami.FormData.label: qsTr("Authorization code:")
        }

        Button {
            text: qsTr("Ok")
            onClicked: authentification.update_authorization_code(authorization_code.text);
        }
    }
}
