/*
 *   Copyright 2019 Carl Schwan <carl@carlschwan.eu>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation; either version 2 of
 *   the License or (at your option) version 3 or any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use qmetaobject::*;
use elefren::{prelude::*, registration::Registered};
use elefren::http_send::{HttpSend, HttpSender};

#[derive(QObject, Default)]
pub struct Authentification {
    base: qt_base_class!(trait QObject),

    // Properties
    auth_url: qt_property!(QString; NOTIFY auth_url_changed),
    error_msg: qt_property!(QString; NOTIFY error_msg_changed),

    // Othe properties (not exposed to QObject
    registered: Option<Registered<HttpSender>>,

    // Signals
    auth_url_changed: qt_signal!(),
    error_msg_changed: qt_signal!(),

    // Methods
    update_instance: qt_method!(fn update_instance(&mut self, instance: String) -> bool {
        match Registration::new(instance).client_name("Krato").build() {
            Err(error) => {
                // Server doesn't exist or is down
                self.auth_url = "".into();
                self.auth_url_changed();
                self.error_msg = error.to_string().into();
                self.error_msg_changed();
                println!("error");
                false
            },
            Ok(registration) => {
                self.registered = Some(registration);
                // Server is ready, get authorization url
                match self.registered.as_ref().unwrap().authorize_url() {
                    Err(error) => {
                        self.auth_url = "".into();
                        self.auth_url_changed();
                        self.error_msg = error.to_string().into();
                        self.error_msg_changed();
                        println!("error");
                        false
                    },
                    Ok(url) => {
                        self.auth_url = url.into();
                        self.auth_url_changed();
                        println!("success");
                        true
                    },
                }
            }
        }
    }),
    update_authorization_code: qt_method!(fn update_authorization_code(&mut self, authorization_code: String) -> bool {
        match self.registered.as_ref().unwrap().complete(&authorization_code) {
            Err(error) => {
                self.error_msg = error.to_string().into();
                self.error_msg_changed();
                false
            },
            Ok(mastodon) => {
                println!("{:?}", mastodon.get_home_timeline().unwrap().initial_items);
                true
            }
        }
    }),
}
