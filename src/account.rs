/*
 *   Copyright 2019 Carl Schwan <carl@carlschwan.eu>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License as
 *   published by the Free Software Foundation; either version 2 of
 *   the License or (at your option) version 3 or any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use qmetaobject::*;
use elefren::{prelude::*, registration::Registered};
use elefren::http_send::{HttpSend, HttpSender};

/// A struct representing an Account.
/// https://docs.rs/elefren/0.20.0/elefren/entities/account/struct.Account.html
#[derive(QObject, Default)]
pub struct Account {
    base: qt_base_class!(trait QObject),

    // Properties
    /// Equals username for local users, includes @domain for remote ones.
    acct: qt_property!(QString; NOTIFY acct_changed),

    /// URL to the avatar image
    avatar: qt_property!(QString; NOTIFY avatar_changed),

    /// URL to the avatar static image (gif)
    avatar_static: qt_property!(QString; NOTIFY avatar_static_changed),

    /// The time the account was created.
    // TODO FIXME: datetime created_at: qt_property!(DATETIME, created_at_changed),

    /// The account's display name.
    display_name: qt_property!(QString; NOTIFY display_name_changed),

    /// The number of followers for the account.
    followers_count: qt_property!(u64; NOTIFY followers_count_changed),

    /// The number of accounts the given account is following.
    following_count: qt_property!(u64; NOTIFY following_count_changed),

    /// URL to the header image.
    header: qt_property!(QString; NOTIFY header_changed),

    /// URL to the header static image (gif).
    header_static: qt_property!(QString; NOTIFY header_static_changed),

    /// The ID of the account.
    id: qt_property!(QString; NOTIFY id_changed),

    /// Boolean for when the account cannot be followed without waiting for approval first.
    locked: qt_property!(bool; NOTIFY locked_changed),

    /// Biography of user.
    note: qt_property!(QString; NOTIFY note_changed),

    /// The number of statuses the account has made.
    statuses_count: qt_property!(u64; NOTIFY statuses_count_changed),

    /// URL of the user's profile page (can be remote).
    url: qt_property!(QString; NOTIFY url_changed),

    /// The username of the account.
    username: qt_property!(QString; NOTIFY username_changed),

    // todo create field for the others attributes

    // Signals
    acct_changed: qt_signal!(),
    avatar_changed: qt_signal!(),
    avatar_static_changed: qt_signal!(),
    // TODO FIXME_changed datetime created_at: qt_signal!(),
    display_name_changed: qt_signal!(),
    followers_count_changed: qt_signal!(),
    following_count_changed: qt_signal!(),
    header_changed: qt_signal!(),
    header_static_changed: qt_signal!(),
    id_changed: qt_signal!(),
    locked_changed: qt_signal!(),
    note_changed: qt_signal!(),
    statuses_count_changed: qt_signal!(),
    url_changed: qt_signal!(),
    username_changed: qt_signal!(),
}
